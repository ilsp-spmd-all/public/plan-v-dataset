import os
import json
import argparse


def load_dict(fp: str):
    with open(fp, "rb") as json_file:
        data_dict = json.load(json_file)
    return data_dict


def parse_arguments():
    parser = argparse.ArgumentParser(description="Main Argument Parser")
    parser.add_argument(
        "--fetch-data",
        action="store_true",
        help="Fetch data flag",
    )
    parser.add_argument(
        "--data-path",
        type=str,
        default="data.json",
        help="Json file full path",
    )
    parser.add_argument(
        "--verbose",
        action="store_true",
        help="Verbose flag: allow printing messages",
    )
    return parser.parse_args()


def get_dataset(data_dict):
    X_speakers, X_transcripts = [], []
    for idx, speaker in enumerate(data_dict.keys()):
        n_stories, n_transcripts = 0, 0
        for story in data_dict[speaker].keys():
            n_stories += 1
            for utterance_id in data_dict[speaker][story].keys():
                n_transcripts += 1
                transcript = data_dict[speaker][story][utterance_id]["text"]
                X_speakers.append(speaker)
                X_transcripts.append(transcript)
        if args.verbose:
            msg = f"{idx+1}) Speaker {speaker}: # Stories = {n_stories}, # Transcripts = {n_transcripts}"
            print(msg)
    return X_speakers, X_transcripts


if __name__ == "__main__":
    args = parse_arguments()

    data_dict = load_dict(fp=args.data_path)

    if args.fetch_data:
        X_speakers, X_transcripts = get_dataset(data_dict)
        if args.verbose:
            print(f"Unique number of speakers: {len(set(X_speakers))}")
