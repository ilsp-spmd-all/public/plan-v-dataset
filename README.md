<p align="center">
  <img src="./planv-logo.jpg" />
</p>

# PLan-V Dataset

# How to get the dataset
The dataset is currently supported in a `json` format. Helper functions are provided in `main.py`. To get an insight of the dataset's content, run:

```
python3 main.py --fetch-data --data-path data.json --verbose
```


# Citation